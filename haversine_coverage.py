import math

# haversine function to get distance in km between two coordinates
# maybe it's futile (?)
def haversine(lat1, lon1, lat2, lon2):
    R = 6372800  # Earth radius in meters    
    phi1, phi2 = math.radians(lat1), math.radians(lat2) 
    dphi       = math.radians(lat2 - lat1)
    dlambda    = math.radians(lon2 - lon1)
    
    a = math.sin(dphi/2)**2 + \
        math.cos(phi1)*math.cos(phi2)*math.sin(dlambda/2)**2
    
    return 2*R*math.atan2(math.sqrt(a), math.sqrt(1 - a))/1000

# list of locations to check
locations = [
	{'id':1000, 'zip_code':'37069', 'lat':45.35, 'lng': 10.84},
	{'id':1001, 'zip_code':'37121', 'lat':45.44, 'lng': 10.99},
	{'id':1002, 'zip_code':'37129', 'lat':45.44, 'lng': 11.00},
  {'id':1003, 'zip_code':'27133', 'lat':45.43, 'lng': 11.02}
];

# list of enabled shoppers 
shoppers = [
	{'id':'S1', 'lat':45.46, 'lng': 11.03, 'enabled': True},
  {'id':'S2', 'lat':45.46, 'lng': 10.12, 'enabled': True},
  {'id':'S3', 'lat':45.34, 'lng': 10.81, 'enabled': True},
	{'id':'S4', 'lat':45.76, 'lng': 10.57, 'enabled': True},
  {'id':'S5', 'lat':45.34, 'lng': 10.63, 'enabled': True},
  {'id':'S6', 'lat':45.42, 'lng': 10.81, 'enabled': True},
  {'id':'S7', 'lat':45.34, 'lng': 10.94, 'enabled': True}  
];

# final sorted list of shoppers with coverages
sorted = [];

# unit of perc to sum 
perc = (100/(len(locations)));

# loop through shoppers
for shopper in shoppers:
  # loop through shoppers  
  for location in locations:
    # check distance between current shopper coordinate and current location coordinate is less than 10km
    if haversine(shopper['lat'],shopper['lng'],location['lat'],location['lng']) < 10:
	    # flag to check if shopper is already in sorted list 
      f = False;
      for s in sorted:
        if s['shopper_id'] == shopper['id']:
          f = True;
          break;
      if f == True:
	    # add coverage's perc unit to the shopper in the sorted list 
        s['coverage']+=perc;
      else:
	    # add shopper to the sorted list
      	sorted.append({'shopper_id':shopper['id'], 'coverage':perc});

# sort the final list        
sorted.sort(key=lambda x: x['coverage'], reverse=True)    

print sorted