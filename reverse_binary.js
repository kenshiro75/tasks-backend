let sourceDecimal = 13
let reversedDecimal = 11

let sourceBin = sourceDecimal.toString(2)


// test
console.log(parseInt(reverseBinary(sourceBin),2)===11)

// my solution: old style :) 
/*
function reverseBinary(sourceBin)
{
  let reverseBin = ''
  for(let x=sourceBin.length-1;x>=0;x--)
  {
    reverseBin+=sourceBin.charAt(x)
  }
  return reverseBin
}
*/
// solution actually found online : much more elegant (?)
function reverseBinary(sourceBin)
{
  return sourceBin.split("").reverse().join("");
}
