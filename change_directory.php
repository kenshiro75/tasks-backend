<?php

// define a class to manage the path
class Path
{
	// path property
    public string $path;
    
	// setting the path while initiating the new object
    public function __construct(string $path)
    {
        $this->path = $path;
    }
    
	// change directory method
    public function cd(string $path)
    {
		// assume the dir separator char is /
        $sep = "/";

		// treating paths as arrays
        $src = explode($sep,$this->path);
        $dest = explode($sep,$path);
        
		// loop through cd dest
        foreach($dest as $sub)
        {
			// if parent directory remove first entry in original path
            if ($sub=='..')
            {
                array_pop($src);
            }
            else { // else push entry in original path
                array_push($src,$sub);
            }
        }
        
		// update class property
        $this->path = implode($sep,$src);
    }
    
	// support method to ger the current path
    public function currentPath()
    {
        return $this->path;
    }
}

$initialPath = '/a/b/c/d';
$path = new Path($initialPath);

// test 1 : check initial path is correctly initiated
echo ($path->currentPath()==$initialPath);
echo PHP_EOL;

// test 2 : check change dir simple path
$path->cd('../x');
echo ($path->currentPath()=='/a/b/c/x');
echo PHP_EOL;

// test 3 : check change dir complex path
$path->cd('../../../../y');
echo ($path->currentPath()=='/y');